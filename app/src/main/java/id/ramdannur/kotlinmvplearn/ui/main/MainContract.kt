package id.ramdannur.kotlinmvplearn.ui.main

import id.ramdannur.kotlinmvplearn.ui.base.BaseContract

/**
 * Created by Ramdannur on 9/4/2018.
 */
class MainContract {
    interface View: BaseContract.View {
        fun showAboutFragment()
        fun showListFragment()
    }

    interface Presenter: BaseContract.Presenter<MainContract.View> {
        fun onDrawerOptionAboutClick()
    }
}