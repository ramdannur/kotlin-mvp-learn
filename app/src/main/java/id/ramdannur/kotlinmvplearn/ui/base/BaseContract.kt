package id.ramdannur.kotlinmvplearn.ui.base

/**
 * Created by Ramdannur on 9/4/2018.
 */
class BaseContract {

    interface Presenter<in T> {
        fun subscribe()
        fun unsubscribe()
        fun attach(view: T)
    }

    interface View {

    }
}