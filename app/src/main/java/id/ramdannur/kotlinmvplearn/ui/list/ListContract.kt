package id.ramdannur.kotlinmvplearn.ui.list

import id.ramdannur.kotlinmvplearn.models.DetailsViewModel
import id.ramdannur.kotlinmvplearn.models.Post
import id.ramdannur.kotlinmvplearn.ui.base.BaseContract

/**
 * Created by Ramdannur on 9/4/2018.
 */
class ListContract {

    interface View: BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun loadDataSuccess(list: List<Post>)
        fun loadDataAllSuccess(model: DetailsViewModel)
    }

    interface Presenter: BaseContract.Presenter<View> {
        fun loadData()
        fun loadDataAll()
        fun deleteItem(item: Post)
    }
}