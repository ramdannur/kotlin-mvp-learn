package id.ramdannur.kotlinmvplearn.models

/**
 * Created by Ramdannur on 9/4/2018.
 */

data class Album(val id: Int, val userId: Int, val title: String)

/*
{
    "userId": 1,
    "id": 1,
    "title": "quidem molestiae enim"
}
 */