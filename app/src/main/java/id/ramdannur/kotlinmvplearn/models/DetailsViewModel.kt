package id.ramdannur.kotlinmvplearn.models

import com.google.gson.Gson

/**
 * Created by Ramdannur on 9/4/2018.
 */
data class DetailsViewModel(val posts: List<Post>, val users: List<User>, val albums: List<Album>) {
    fun toJson(): String {
        return Gson().toJson(this)
    }
}