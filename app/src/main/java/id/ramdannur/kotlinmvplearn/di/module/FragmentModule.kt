package id.ramdannur.kotlinmvplearn.di.module

import dagger.Module
import dagger.Provides
import id.ramdannur.kotlinmvplearn.api.ApiServiceInterface
import id.ramdannur.kotlinmvplearn.ui.about.AboutContract
import id.ramdannur.kotlinmvplearn.ui.about.AboutPresenter
import id.ramdannur.kotlinmvplearn.ui.list.ListContract
import id.ramdannur.kotlinmvplearn.ui.list.ListPresenter

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Module
class FragmentModule {

    @Provides
    fun provideAboutPresenter(): AboutContract.Presenter {
        return AboutPresenter()
    }

    @Provides
    fun provideListPresenter(): ListContract.Presenter {
        return ListPresenter()
    }

    @Provides
    fun provideApiService(): ApiServiceInterface {
        return ApiServiceInterface.create()
    }
}