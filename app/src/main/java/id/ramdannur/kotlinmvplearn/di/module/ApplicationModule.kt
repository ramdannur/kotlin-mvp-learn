package id.ramdannur.kotlinmvplearn.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import id.ramdannur.kotlinmvplearn.BaseApp
import id.ramdannur.kotlinmvplearn.di.scope.PerApplication
import javax.inject.Singleton

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Module
class ApplicationModule(private val baseApp: BaseApp) {

    @Provides
    @Singleton
    @PerApplication
    fun provideApplication(): Application {
        return baseApp
    }
}