package id.ramdannur.kotlinmvplearn.di.component

import dagger.Component
import id.ramdannur.kotlinmvplearn.BaseApp
import id.ramdannur.kotlinmvplearn.di.module.ApplicationModule

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(application: BaseApp)

}