package id.ramdannur.kotlinmvplearn.di.component

import dagger.Component
import id.ramdannur.kotlinmvplearn.di.module.FragmentModule
import id.ramdannur.kotlinmvplearn.ui.about.AboutFragment
import id.ramdannur.kotlinmvplearn.ui.list.ListFragment

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Component(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

    fun inject(aboutFragment: AboutFragment)

    fun inject(listFragment: ListFragment)

}