package id.ramdannur.kotlinmvplearn.di.component

import dagger.Component
import id.ramdannur.kotlinmvplearn.di.module.ActivityModule
import id.ramdannur.kotlinmvplearn.ui.main.MainActivity

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Component(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

}