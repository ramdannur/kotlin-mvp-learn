package id.ramdannur.kotlinmvplearn.di.module

import android.app.Activity
import dagger.Module
import dagger.Provides
import id.ramdannur.kotlinmvplearn.ui.main.MainContract
import id.ramdannur.kotlinmvplearn.ui.main.MainPresenter

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Module
class ActivityModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter()
    }

}