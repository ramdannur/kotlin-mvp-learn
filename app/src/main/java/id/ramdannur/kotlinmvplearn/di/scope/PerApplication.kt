package id.ramdannur.kotlinmvplearn.di.scope

import javax.inject.Qualifier

/**
 * Created by Ramdannur on 9/4/2018.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication