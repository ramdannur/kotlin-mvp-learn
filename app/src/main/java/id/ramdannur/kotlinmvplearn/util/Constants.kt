package id.ramdannur.kotlinmvplearn.util

/**
 * Created by Ramdannur on 9/4/2018.
 */

class Constants{
    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }
}