package id.ramdannur.kotlinmvplearn

import android.app.Application
import id.ramdannur.kotlinmvplearn.di.component.ApplicationComponent
import id.ramdannur.kotlinmvplearn.di.component.DaggerApplicationComponent
import id.ramdannur.kotlinmvplearn.di.module.ApplicationModule

/**
 * Created by Ramdannur on 9/4/2018.
 */

class BaseApp: Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        setup()

        if (BuildConfig.DEBUG) {
            // Maybe TimberPlant etc.
        }
    }

    fun setup() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: BaseApp private set
    }

}